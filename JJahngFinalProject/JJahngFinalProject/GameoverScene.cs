﻿/*
 * GameoverScene.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 *          Jay Jahng 12/10/2018: Finished
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JJahngFinalProject
{
    /// <summary>
    /// Class for displaying GameoverScene
    /// </summary>
    public class GameoverScene : GameScene
    {
        SpriteBatch spriteBatch;
        Texture2D gameoverImage;
        SpriteFont scoreFont;
        Color fontColor = Color.White;
        Vector2 score1Pos = new Vector2(600, 358);
        Vector2 score2Pos = new Vector2(600, 425);
        private bool sceneType;

        private double knightScore;
        private double knightHighScore;
        private double gunnerScore;
        private double gunnerHighScore;
        
        /// <summary>
        /// current knight level's score 
        /// </summary>
        public double KnightScore { get => knightScore; set => knightScore = value; }

        /// <summary>
        /// current best score for knight level
        /// </summary>
        public double KnightHighScore { get => knightHighScore; set => knightHighScore = value; }

        /// <summary>
        /// current gunner levle's score
        /// </summary>
        public double GunnerScore { get => gunnerScore; set => gunnerScore = value; }

        /// <summary>
        /// current best score for gunner level
        /// </summary>
        public double GunnerHighScore { get => gunnerHighScore; set => gunnerHighScore = value; }

        /// <summary>
        /// Constructor of GameoverScene
        /// Load gameoverImage and scoreFont
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="sceneType"></param>
        public GameoverScene(Game game,
            SpriteBatch spriteBatch,
            bool sceneType) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.sceneType = sceneType;
            this.gameoverImage = game.Content.Load<Texture2D>("Image/Other/Gameover");
            this.scoreFont = game.Content.Load<SpriteFont>("Font/regularFont");
        }

        /// <summary>
        /// Draw method for GameoverScene
        /// Draw gameoverImage
        /// if sceneType is true
        ///     Draw knight level's current score and best highscore
        /// if sceneType is false
        ///     Draw gunner level's current score and best highscore
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(gameoverImage, Vector2.Zero, Color.White);
            if (sceneType)
            {
                spriteBatch.DrawString(scoreFont, knightScore.ToString(), score1Pos, fontColor);
                spriteBatch.DrawString(scoreFont, knightHighScore.ToString(), score2Pos, fontColor);
            }
            else
            {
                spriteBatch.DrawString(scoreFont, gunnerScore.ToString(), score1Pos, fontColor);
                spriteBatch.DrawString(scoreFont, gunnerHighScore.ToString(), score2Pos, fontColor);
            }

            spriteBatch.DrawString(scoreFont, "Press Enter to restart", new Vector2(400, 465), fontColor);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
