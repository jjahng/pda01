﻿/*
 * Game1.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 *          Jay Jahng 12/10/2018: Finished
 */
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;

namespace JJahngFinalProject
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        //declare all scenes here
        private StartScene startScene;
        private KnightScene knightScene;
        private GunnerScene gunnerScene;
        private GameoverScene knightGameoverScene;
        private GameoverScene gunnerGameoverScene;
        private HighscoreScene highscoreScene;
        private HelpScene helpScene;
        private CreditScene creditScene;
        //declaration ends
        private double knightHighScore; //highscore for knight level
        private double gunnerHighScore; //highscore for gunner level

        /// <summary>
        /// set game window size when it starts
        /// </summary>
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 1000;  // set width value of the game window
            graphics.PreferredBackBufferHeight = 500;   // set height value of the game window
            graphics.ApplyChanges();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// Set Share.stage
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Share.stage = new Vector2(graphics.PreferredBackBufferWidth,
                graphics.PreferredBackBufferHeight);
            System.Console.WriteLine(Share.stage.X + "  " + Share.stage.Y);
            SaveOrLoad(false);
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// Loads all scenes
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            // instantiate all scenes here
            startScene = new StartScene(this, spriteBatch);
            this.Components.Add(startScene);

            knightScene = new KnightScene(this, spriteBatch);
            this.Components.Add(knightScene);

            gunnerScene = new GunnerScene(this, spriteBatch);
            this.Components.Add(gunnerScene);

            knightGameoverScene = new GameoverScene(this, spriteBatch, true);
            this.Components.Add(knightGameoverScene);
            knightGameoverScene.KnightHighScore = knightHighScore;

            gunnerGameoverScene = new GameoverScene(this, spriteBatch, false);
            this.Components.Add(gunnerGameoverScene);
            gunnerGameoverScene.GunnerHighScore = gunnerHighScore;

            highscoreScene = new HighscoreScene(this, spriteBatch);
            this.Components.Add(highscoreScene);

            helpScene = new HelpScene(this, spriteBatch);
            this.Components.Add(helpScene);

            creditScene = new CreditScene(this, spriteBatch);
            this.Components.Add(creditScene);

            //instantiation ends
            //make startScene visible/enabled
            startScene.show();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// Displays only desired scene, one scene at a time
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            //    Exit();

            int selectedIndex = 0;
            KeyboardState ks = Keyboard.GetState();
            if (startScene.Enabled)
            {
                selectedIndex = startScene.Menu.SelectedIndex;
                if (selectedIndex == 0 && ks.IsKeyDown(Keys.Enter))
                {
                    startScene.hide();
                    knightScene.show();
                }
                if (selectedIndex == 1 && ks.IsKeyDown(Keys.Enter))
                {
                    startScene.hide();
                    gunnerScene.show();
                }
                if (selectedIndex == 2 && ks.IsKeyDown(Keys.Enter))
                {
                    startScene.hide();
                    highscoreScene.show();
                    highscoreScene.KnightScore = knightHighScore;
                    highscoreScene.GunnerScore = gunnerHighScore;
                }
                if (selectedIndex == 3 && ks.IsKeyDown(Keys.Enter))
                {
                    startScene.hide();
                    helpScene.show();
                }
                if (selectedIndex == 4 && ks.IsKeyDown(Keys.Enter))
                {
                    startScene.hide();
                    creditScene.show();
                }
                if (selectedIndex == 5 && ks.IsKeyDown(Keys.Enter))
                {
                    SaveOrLoad(true);
                    Exit();
                }
            }
            if (knightScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    knightScene.BgmInstance.Pause();
                    knightScene.hide();
                    startScene.show();
                }
                if (knightScene.Character.GameOver)
                {
                    knightScene.BgmInstance.Pause();
                    knightGameoverScene.KnightScore = knightScene.Character.Score;
                    if (knightGameoverScene.KnightScore > knightHighScore)
                    {
                        knightHighScore = knightGameoverScene.KnightScore;
                        knightGameoverScene.KnightHighScore = knightHighScore;
                    }
                    knightScene.hide();
                    knightScene.Dispose();
                    knightGameoverScene.show();
                }
            }
            if (gunnerScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    gunnerScene.BgmInstance.Pause();
                    gunnerScene.hide();
                    startScene.show();
                }
                if (gunnerScene.Character.GameOver)
                {
                    gunnerScene.BgmInstance.Pause();
                    gunnerGameoverScene.GunnerScore = gunnerScene.Character.Score;
                    if (gunnerGameoverScene.GunnerScore > gunnerHighScore)
                    {
                        gunnerHighScore = gunnerGameoverScene.GunnerScore;
                        gunnerGameoverScene.GunnerHighScore = gunnerHighScore;
                    }
                    gunnerScene.hide();
                    gunnerScene.Dispose();
                    gunnerGameoverScene.show();
                }
            }
            if (knightGameoverScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    knightGameoverScene.hide();
                    startScene.show();
                }
                if (ks.IsKeyDown(Keys.Enter))
                {
                    knightScene = new KnightScene(this, spriteBatch);
                    this.Components.Add(knightScene);
                    knightGameoverScene.hide();
                    knightScene.show();
                }
            }
            if (gunnerGameoverScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    gunnerGameoverScene.hide();
                    startScene.show();
                }
                if (ks.IsKeyDown(Keys.Enter))
                {
                    gunnerScene = new GunnerScene(this, spriteBatch);
                    this.Components.Add(gunnerScene);
                    gunnerGameoverScene.hide();
                    gunnerScene.show();
                }
            }
            if (highscoreScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    highscoreScene.hide();
                    startScene.show();
                }
            }
            if (helpScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    helpScene.hide();
                    startScene.show();
                }
            }
            if (creditScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    creditScene.hide();
                    startScene.show();
                }
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// Make default background black
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }

        /// <summary>
        /// if true saves highscores to txtfile
        /// if false load highscores to this.knightHighScore and this.gunnerHighScore
        /// </summary>
        /// <param name="save"></param>
        private void SaveOrLoad(bool save)
        {
            if (save)
            {
                using (StreamWriter sw = new StreamWriter("saveFile.txt", false))
                {
                    sw.WriteLine(knightHighScore);
                    sw.WriteLine(gunnerHighScore);
                }
            }
            else
            {
                if (File.Exists("saveFile.txt"))
                {
                    using (StreamReader sr = new StreamReader("saveFile.txt"))
                    {
                        knightHighScore = double.Parse(sr.ReadLine());
                        gunnerHighScore = double.Parse(sr.ReadLine());
                    }
                }
            }   
        }
    }
}
