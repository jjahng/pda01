﻿/*
 * BackgroundScrolling.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 *          Jay Jahng 12/10/2018: Finished
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JJahngFinalProject
{
    /// <summary>
    /// Class for creating scrolling background
    /// </summary>
    public class BackgroundScrolling : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Texture2D tex;
        private Vector2 position1, position2;
        private Vector2 speed;

        /// <summary>
        /// BackgroundScrolling's constructor
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="tex"></param>
        /// <param name="position"></param>
        /// <param name="speed"></param>
        public BackgroundScrolling(Game game,
            SpriteBatch spriteBatch, Texture2D tex, Vector2 position, Vector2 speed) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.tex = tex;
            this.position1 = position;
            this.position2 = new Vector2(position1.X + tex.Width, position1.Y);
            this.speed = speed;
        }

        /// <summary>
        /// Draw method of BackGroundScrolling
        /// Draw two background tiles in different position
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(tex, position1, Color.White);
            spriteBatch.Draw(tex, position2, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// update(scroll) background with share.speed
        /// if background tile go out of the screen position it to the other side of the screen
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            position1 += Share.speed / 3;
            position2 += Share.speed / 3;

            if (position1.X < -tex.Width)
            {
                position1.X = position2.X + tex.Width;
            }

            if (position2.X < -tex.Width)
            {
                position2.X = position1.X + tex.Width;
            }
            base.Update(gameTime);
        }
    }
}
