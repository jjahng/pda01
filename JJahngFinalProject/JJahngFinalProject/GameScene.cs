﻿/*
 * GameScene.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JJahngFinalProject
{
    /// <summary>
    /// Base class for all GameScenes
    /// </summary>
    public class GameScene : DrawableGameComponent
    {
        private List<GameComponent> components;

        /// <summary>
        /// List of scenes Components
        /// </summary>
        public List<GameComponent> Components { get => components; set => components = value; }

        /// <summary>
        /// display scene
        /// </summary>
        public virtual void show()
        {
            this.Enabled = true;
            this.Visible = true;
        }
        
        /// <summary>
        /// hide scene
        /// </summary>
        public virtual void hide()
        {
            this.Enabled = false;
            this.Visible = false;
        }

        /// <summary>
        /// constructor for GameScene
        /// </summary>
        /// <param name="game"></param>
        public GameScene(Game game) : base(game)
        {
            components = new List<GameComponent>();
            hide();
        }

        /// <summary>
        /// Draw if item in components List is DrawableGameComponent
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            DrawableGameComponent comp = null;
            foreach (GameComponent item in components)
            {
                if (item is DrawableGameComponent)
                {
                    comp = (DrawableGameComponent)item;
                    if (comp.Visible)
                    {
                        comp.Draw(gameTime);
                    }
                }
            }
            base.Draw(gameTime);
        }

        /// <summary>
        /// Update item in components List
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            //foreach (GameComponent item in components)
            //{
            //    if (item.Enabled)
            //    {
            //        item.Update(gameTime);
            //    }
            //} // Cannot use this because components changes during iteration
            for (int i = 0; i < components.Count; i++)
            {
                if (components[i].Enabled)
                {
                    components[i].Update(gameTime);
                }
            }
            base.Update(gameTime);
        }
    }
}
