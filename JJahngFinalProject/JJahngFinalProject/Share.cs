﻿/*
 * Game1.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 *          Jay Jahng 12/10/2018: Finished
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace JJahngFinalProject
{
    /// <summary>
    /// Class for saving shared variables
    /// </summary>
    public static class Share
    {
        public static Vector2 stage;
        public static Vector2 speed;
    }
}
