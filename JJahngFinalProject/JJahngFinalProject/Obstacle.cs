﻿/*
 * Game1.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 *          Jay Jahng 12/10/2018: Finished
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace JJahngFinalProject
{
    /// <summary>
    /// Class for displaying obstacles
    /// </summary>
    public class Obstacle : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Texture2D woodTex, steelTex;
        private Vector2 position;
        private Character character; //for saving current character object
        private SoundEffect woodCrack;
        private Rectangle charSpearRect, charBodyRect, boxRect;
        private int type;

        /// <summary>
        /// constructor of Obstacle
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="woodTex"></param>
        /// <param name="steelTex"></param>
        /// <param name="character"></param>
        /// <param name="type"></param>
        /// <param name="woodCrack"></param>
        public Obstacle(Game game,
            SpriteBatch spriteBatch,
            Texture2D woodTex,
            Texture2D steelTex,
            Character character,
            int type,
            SoundEffect woodCrack) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.woodTex = woodTex;
            this.steelTex = steelTex;
            this.character = character;
            this.type = type;
            this.woodCrack = woodCrack;
            position = new Vector2(1000, 350 - woodTex.Height);

        }

        /// <summary>
        /// Draw method of Obstacle
        /// display different tex, depends on type
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            if (type == 0)
            {
                spriteBatch.Draw(woodTex, position, Color.White);
            }
            else
            {
                spriteBatch.Draw(steelTex, position, Color.White);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Update method of Obstacle
        /// handles collision with the character
        /// different out come, depends on character status and box type
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            charBodyRect = new Rectangle((int)character.Position.X, (int)character.Position.Y, character.IdleTex.Width / 2 - 105, character.IdleTex.Height);
            charSpearRect = new Rectangle((int)character.Position.X, (int)character.Position.Y, character.IdleTex.Width / 2, character.IdleTex.Height);
            boxRect = new Rectangle((int)position.X, (int)position.Y, woodTex.Width, woodTex.Height);
            position += Share.speed; //moving with ground

            //if character body hits box, call character.Hit() method
            if (boxRect.Intersects(charBodyRect) && !character.WasHit)
            {
                if(character.Position.X + charBodyRect.Width <= this.position.X + (boxRect.Width / 2))
                {
                    character.Hit(gameTime);
                    woodCrack.Play();
                    this.Visible = false;
                    this.Enabled = false;
                }
            }

            //cracking box with character dash if it's woodenbox + speed up
            if (boxRect.Intersects(charSpearRect) && !character.WasHit)
            {
                if (character.DashEnabled && type == 0)
                {
                    character.Speed *= 1.1f;
                    woodCrack.Play();
                    this.Visible = false;
                    this.Enabled = false;
                }
            }
            base.Update(gameTime);
        }
    }
}
