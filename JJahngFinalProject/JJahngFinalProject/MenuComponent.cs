﻿/*
 * Game1.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 *          Jay Jahng 12/10/2018: Finished
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JJahngFinalProject
{
    /// <summary>
    /// class for displaying menu components
    /// </summary>
    public class MenuComponent : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private SpriteFont regularFont, highlightFont;
        private Texture2D bgForestTex, bgCityTex;
        private List<string> menuItems;
        public int SelectedIndex { get; set; } = 0;
        private Vector2 position;
        private Color regularColor = Color.Gold;
        private Color hilightColor = Color.White;

        private KeyboardState oldState;

        /// <summary>
        /// constructor of MenuComponent
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="regularFont"></param>
        /// <param name="highlightFont"></param>
        /// <param name="menus"></param>
        /// <param name="bgForestTex"></param>
        /// <param name="bgCityTex"></param>
        public MenuComponent(Game game,
            SpriteBatch spriteBatch,
            SpriteFont regularFont,
            SpriteFont highlightFont,
            string[] menus,
            Texture2D bgForestTex,
            Texture2D bgCityTex) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.regularFont = regularFont;
            this.highlightFont = highlightFont;
            menuItems = menus.ToList<string>();
            this.bgForestTex = bgForestTex;
            this.bgCityTex = bgCityTex;
            position = new Vector2(Share.stage.X / 2 + 100, Share.stage.Y / 2 - 100);

        }

        /// <summary>
        /// Draw method of MenuComponents
        /// Display menu components with one selected component in highlightFont
        /// Display background if knight level or gunner level menu item is selected
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            Vector2 tempPos = position;
            spriteBatch.Begin();

            if (SelectedIndex == 0)
            {
                spriteBatch.Draw(bgForestTex, Vector2.Zero, Color.White);
            }

            if (SelectedIndex == 1)
            {
                spriteBatch.Draw(bgCityTex, Vector2.Zero, Color.White);
            }

            for (int i = 0; i < menuItems.Count; i++)
            {
                if (SelectedIndex == i)
                {
                    spriteBatch.DrawString(highlightFont, menuItems[i], tempPos, hilightColor);
                    tempPos.Y += highlightFont.LineSpacing;
                }
                else
                {
                    spriteBatch.DrawString(regularFont, menuItems[i], tempPos, regularColor);
                    tempPos.Y += regularFont.LineSpacing;
                }
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Update method of MenuComponents
        /// handles selecting menu item
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Down) && oldState.IsKeyUp(Keys.Down))
            {
                SelectedIndex++;
                if (SelectedIndex == menuItems.Count)
                {
                    SelectedIndex = 0;
                }
            }
            if (ks.IsKeyDown(Keys.Up) && oldState.IsKeyUp(Keys.Up))
            {
                SelectedIndex--;
                if (SelectedIndex == -1)
                {
                    SelectedIndex = menuItems.Count - 1;
                }
            }

            oldState = ks;
            base.Update(gameTime);
        }
    }
}
