﻿/*
 * Character.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 *          Jay Jahng 12/10/2018: Finished
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace JJahngFinalProject
{
    /// <summary>
    /// Class for creating character
    /// </summary>
    public class Character : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Texture2D idleTex, moveTex, heartTex;
        private int frameIndex = 0;
        private Vector2 dimension; //dimension of a character animaiton frame
        private Vector2 heartDimension;
        private int hitStatus = 0;
        private List<Rectangle> idleFrames;
        private List<Rectangle> moveFrames;
        private List<Rectangle> lifeFrames;
        private Vector2 initPosition;
        private KeyboardState oldState;
        private double clickTime; //time of last pressed D
        private float delay; //frame gap between each animation
        private float delayCounter = 0;
        private bool rightIsUp = true; //Is D up?
        private bool jumped = false; //Is character jumped?
        private double hitTime; //to make character invulnerable for certain amount of time when hit
        private SpriteFont scoreFont;
        private Color scoreColor = Color.White;
        private SoundEffect footStep;


        const double TIMER_DELAY = 0.4; //delay for checking double click as Constant
        const double HIT_DELAY = 2; //time amount of invulnerable state in seconds as Constant

        private Vector2 position;
        private float speed = -7;
        private bool isGrounded = true;
        private bool dashEnabled = false;
        private bool wasHit = false;
        private bool gameOver = false;
        private double score = 0;

        /// <summary>
        /// character's idle texture
        /// </summary>
        public Texture2D IdleTex { get => idleTex; set => idleTex = value; }

        /// <summary>
        /// character's current Position
        /// </summary>
        public Vector2 Position { get => position; set => position = value; }

        /// <summary>
        /// character's speed
        /// </summary>
        public float Speed { get => speed; set => speed = value; }

        /// <summary>
        /// is character grounded?
        /// </summary>
        public bool IsGrounded { get => isGrounded; set => isGrounded = value; }

        /// <summary>
        /// is character currently dashing forward?
        /// </summary>
        public bool DashEnabled { get => dashEnabled; set => dashEnabled = value; }

        /// <summary>
        /// was character hit?
        /// </summary>
        public bool WasHit { get => wasHit; set => wasHit = value; }

        /// <summary>
        /// is character dead?
        /// </summary>
        public bool GameOver { get => gameOver; set => gameOver = value; }

        /// <summary>
        /// Score of current game
        /// </summary>
        public double Score { get => score; set => score = value; }

        /// <summary>
        /// constructer of Character
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="idleTex"></param>
        /// <param name="moveTex"></param>
        /// <param name="heartTex"></param>
        /// <param name="pos"></param>
        /// <param name="scoreFont"></param>
        /// <param name="footStep"></param>
        public Character(Game game,
            SpriteBatch spriteBatch,
            Texture2D idleTex,
            Texture2D moveTex,
            Texture2D heartTex,
            Vector2 pos,
            SpriteFont scoreFont,
            SoundEffect footStep
            ) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.idleTex = idleTex;
            this.moveTex = moveTex;
            initPosition = pos;
            this.position = initPosition;
            this.idleTex = idleTex;
            this.moveTex = moveTex;
            this.heartTex = heartTex;
            this.scoreFont = scoreFont;
            this.footStep = footStep;
            dimension = new Vector2(271, 200);
            heartDimension = new Vector2(160, 45);
            CreateFrames();
            delay = 9;
        }

        /// <summary>
        /// Draw method of Character
        /// Draw idle animation when stopped
        /// Draw move animation when moving
        /// Draw jump animaition when it's not grounded
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            if (Share.speed.X == 0 && isGrounded)
            {
                if (frameIndex > 1)
                {
                    frameIndex = 0;
                }
                spriteBatch.Draw(idleTex, position, idleFrames.ElementAt<Rectangle>(frameIndex), Color.White);
            }
            else if (Share.speed.X != 0 && isGrounded)
            {
                spriteBatch.Draw(moveTex, position, moveFrames.ElementAt<Rectangle>(frameIndex), Color.White);
            }
            else if (!isGrounded)
            {
                spriteBatch.Draw(moveTex, position, moveFrames.ElementAt<Rectangle>(0), Color.White);
            }

            if (hitStatus < 3)
            {
                spriteBatch.Draw(heartTex, new Vector2(800, 20), lifeFrames.ElementAt<Rectangle>(hitStatus), Color.White);
            }

            spriteBatch.DrawString(scoreFont, "Score:" + score, new Vector2(30, 20), Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Update method of Character
        /// set Share.speed as this.speed when D is pressed
        /// dash when d was double clicked
        /// jump when character is grounded and space was pressed
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            delayCounter++;
            KeyboardState ks = Keyboard.GetState();
            
            if (ks.IsKeyDown(Keys.D))
            {
                //Checking for double click
                if (oldState != ks && gameTime.TotalGameTime.TotalSeconds - clickTime < TIMER_DELAY && rightIsUp)
                {
                    dashEnabled = true;
                    oldState = ks;
                }
                //normal press
                else
                {
                    Share.speed = new Vector2(speed, 0);
                    oldState = ks;
                    clickTime = gameTime.TotalGameTime.TotalSeconds;
                    rightIsUp = false;
                }
            }

            if (ks.IsKeyUp(Keys.D))
            {
                oldState = ks;
                rightIsUp = true;
                if (gameTime.TotalGameTime.TotalSeconds - clickTime  > TIMER_DELAY)
                {
                    Share.speed = Vector2.Zero;
                }
            }

            if (ks.IsKeyDown(Keys.Space) && isGrounded)
            {
                jumped = true;
                isGrounded = false;
            }

            //for rising and falling
            if (jumped)
            {
                Jump();
                if (position.Y <= -150)
                {
                    jumped = false;
                }
            }
            else
            {
                Fall();
                if (position.Y >= 145)
                {
                    position.Y = 150;
                    isGrounded = true;
                }
            }                
            
            //change frameindex
            if (delayCounter > delay)
            {
                score -= Share.speed.X / 5;
                score = Math.Round(score, 0);
                if (Share.speed.X == 0 && isGrounded)
                {
                    frameIndex++;
                    if (frameIndex > 1)
                    {
                        frameIndex = 0;
                    }
                }
                else if (Share.speed.X != 0 && isGrounded)
                {
                    frameIndex++;
                    footStep.Play();
                    if (frameIndex > 3)
                    {
                        frameIndex = 0;
                    }
                }

                //make character blink
                if (wasHit)
                {
                    if (this.Visible == true)
                    {
                        this.Visible = false;
                    }
                    else
                    {
                        this.Visible = true;
                    }
                }
                else
                {
                    this.Visible = true;
                }
                delayCounter = 0;
            }

            //handles dash movement
            if (dashEnabled)
            {
                MoveChar(new Vector2(400, 350 - idleTex.Height));
            }
            else
            {
                MoveChar(new Vector2(200, 350 - idleTex.Height));
            }

            //stops blinking after HIT_DELAY
            if (wasHit)
            {
                if (gameTime.TotalGameTime.TotalSeconds - hitTime > HIT_DELAY)
                {
                    wasHit = false;
                }
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// Create animaition frames from textures
        /// </summary>
        private void CreateFrames()
        {
            idleFrames = new List<Rectangle>();
            for (int i = 0; i < 2; i++)
            {
                int x = i * (int)dimension.X;
                Rectangle r = new Rectangle(x, 0, (int)dimension.X, (int)dimension.Y);

                idleFrames.Add(r);
            }

            moveFrames = new List<Rectangle>();
            for (int i = 0; i < 4; i++)
            {
                int x = i * (int)dimension.X;
                Rectangle r = new Rectangle(x, 0, (int)dimension.X, (int)dimension.Y);

                moveFrames.Add(r);
            }

            //for showing life
            lifeFrames = new List<Rectangle>();
            for (int i = 0; i < 3; i++)
            {
                int x = i * (int)heartDimension.X;
                Rectangle r = new Rectangle(x, 0, (int)heartDimension.X, (int)heartDimension.Y);

                lifeFrames.Add(r);
            }
        }

        /// <summary>
        /// Move character to the paramater target's position
        /// </summary>
        /// <param name="target"></param>
        private void MoveChar(Vector2 target)
        {
            float xDiff = target.X - position.X;
            
            position.X += xDiff * 0.5f;
            if (position.X >= 390)
            {
                dashEnabled = false;
            }
        }

        /// <summary>
        /// make character rise
        /// </summary>
        private void Jump()
        {
            position.Y -= 15;
        }

        /// <summary>
        /// make character fall
        /// </summary>
        private void Fall()
        {
            position.Y += 12;
        }

        /// <summary>
        /// change character to hit state, increase hitstatus and decrease speed
        /// </summary>
        /// <param name="gameTime"></param>
        public void Hit(GameTime gameTime)
        {
            hitTime = gameTime.TotalGameTime.TotalSeconds;
            wasHit = true;
            score -= 100;
            speed *= 0.9f;
            if (speed > -7)
            {
                speed = -7;
            }
            if (score < 0)
            {
                score = 0;
            }
            hitStatus++;
            if (hitStatus > 2)
            {
                gameOver = true;
            }
        }
    }
}
