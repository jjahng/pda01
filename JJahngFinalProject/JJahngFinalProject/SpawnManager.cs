﻿/*
 * Game1.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 *          Jay Jahng 12/10/2018: Finished
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace JJahngFinalProject
{
    /// <summary>
    /// class for spawning boxes
    /// </summary>
    public class SpawnManager : GameComponent
    {
        private Game game;
        private SpriteBatch spriteBatch;
        private GameScene gameScene; //to access current GameScene's Components list
        private Texture2D woodTex, steelTex;
        private Character character;
        private Obstacle obstacle;
        private SoundEffect woodCrack;
        private double delayTimer = 0;
        private Random rndm;
        private int rndmDelay;
        private int minDelay;
        private int maxDelay;
        private int rndmType;
        private float charMoveDis;

        /// <summary>
        /// SpawnManager's constructor
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="gameScene"></param>
        /// <param name="woodTex"></param>
        /// <param name="steelTex"></param>
        /// <param name="character"></param>
        /// <param name="woodCrack"></param>
        /// <param name="minDelay"></param>
        /// <param name="maxDelay"></param>
        public SpawnManager(Game game,
            SpriteBatch spriteBatch,
            GameScene gameScene,
            Texture2D woodTex,
            Texture2D steelTex,
            Character character,
            SoundEffect woodCrack,
            int minDelay,
            int maxDelay) : base(game)
        {
            this.game = game;
            this.spriteBatch = spriteBatch;
            this.gameScene = gameScene;
            this.woodTex = woodTex;
            this.steelTex = steelTex;
            this.character = character;
            this.woodCrack = woodCrack;
            this.minDelay = minDelay;
            this.maxDelay = maxDelay;
            rndm = new Random();
            rndmDelay = rndm.Next(minDelay, maxDelay);
            rndmType = rndm.Next(0, 2);
            charMoveDis = 0;
        }

        /// <summary>
        /// Update method of spawnmanager
        /// if random time passed and character moved certain length spawn box randomly
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            charMoveDis -= Share.speed.X * 0.05f;
            //Spawn
            if (gameTime.TotalGameTime.TotalSeconds - delayTimer > rndmDelay)
            {
                if (charMoveDis > rndmDelay * 10)
                {
                    rndmDelay = rndm.Next(minDelay, maxDelay); //randomly select next time delay
                    rndmType = rndm.Next(0, 2); //randomly select box type
                    obstacle = new Obstacle(game, spriteBatch, woodTex, steelTex, character, rndmType, woodCrack); //spawn box
                    delayTimer = gameTime.TotalGameTime.TotalSeconds;
                    gameScene.Components.Add(obstacle);
                    charMoveDis = 0;
                }
            }
            base.Update(gameTime);
        }
    }
}
