﻿/*
 * Game1.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 *          Jay Jahng 12/10/2018: Finished
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JJahngFinalProject
{
    /// <summary>
    /// Class for dislaying highscore scene
    /// </summary>
    public class HighscoreScene : GameScene
    {
        private SpriteBatch spriteBatch;
        private Texture2D highscoreImage;
        private SpriteFont scoreFont;
        private Color scoreColor = Color.White;
        private Vector2 knightScorePos = new Vector2(170, 300);
        private Vector2 gunnerScorePos = new Vector2(700, 300);

        private double knightScore, gunnerScore;

        /// <summary>
        /// To set high score of Knight level
        /// </summary>
        public double KnightScore { get => knightScore; set => knightScore = value; }

        /// <summary>
        /// To set high score of Gunner level
        /// </summary>
        public double GunnerScore { get => gunnerScore; set => gunnerScore = value; }

        /// <summary>
        /// HighscoreScene's constructor
        /// load highscoreImage and font
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        public HighscoreScene(Game game, SpriteBatch spriteBatch) : base(game)
        {
            this.spriteBatch = spriteBatch;
            highscoreImage = game.Content.Load<Texture2D>("Image/Other/Highscore");
            scoreFont = game.Content.Load<SpriteFont>("Font/highlightFont");
        }

        /// <summary>
        /// Draw method of HighScoreScene
        /// draw highscoreImage with score
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(highscoreImage, Vector2.Zero, scoreColor);
            spriteBatch.DrawString(scoreFont, knightScore.ToString(), knightScorePos, scoreColor);
            spriteBatch.DrawString(scoreFont, gunnerScore.ToString(), gunnerScorePos, scoreColor);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
