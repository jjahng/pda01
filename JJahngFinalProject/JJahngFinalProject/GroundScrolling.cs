﻿/*
 * GroungScrolling.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 *          Jay Jahng 12/10/2018: Finished
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JJahngFinalProject
{
    /// <summary>
    /// Class for creating scrolling ground
    /// </summary>
    public class GroundScrolling : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Texture2D tex1, tex2;
        private Vector2 position1, position2;
        private Vector2 speed;

        /// <summary>
        /// Constructor of GroundScrolling
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="tex1"></param>
        /// <param name="tex2"></param>
        /// <param name="position"></param>
        /// <param name="speed"></param>
        public GroundScrolling(Game game,
            SpriteBatch spriteBatch, Texture2D tex1, Texture2D tex2, Vector2 position, Vector2 speed) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.tex1 = tex1;
            this.tex2 = tex2;
            this.position1 = position;
            this.position2 = new Vector2(position1.X + tex1.Width, position1.Y);
            this.speed = speed;
        }

        /// <summary>
        /// Draw method of GroundScrolling
        /// draw ground twice in different position
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(tex1, position1, Color.White);
            spriteBatch.Draw(tex2, position2, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Update method for GroundScrolling
        /// Move(scroll) ground tile with Share.speed
        /// If ground tile is out of screen move it to the other end of the screen
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            position1 += Share.speed;
            position2 += Share.speed;

            if (position1.X < -tex1.Width)
            {
                position1.X = position2.X + tex1.Width;
            }

            if (position2.X < -tex1.Width)
            {
                position2.X = position1.X + tex1.Width;
            }
            base.Update(gameTime);
        }
    }
}
