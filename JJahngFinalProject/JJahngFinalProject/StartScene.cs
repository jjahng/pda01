﻿/*
 * Game1.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 *          Jay Jahng 12/10/2018: Finished
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JJahngFinalProject
{
    /// <summary>
    /// class for displaying Start scene
    /// </summary>
    public class StartScene : GameScene
    {
        private SpriteBatch spriteBatch;
        public MenuComponent Menu { get; set; }

        //Menu items
        private string[] menuItems =
        {
            "Start Knight Level",
            "Start Gunner Level",
            "High Score",
            "Help",
            "Credit",
            "Quit"
        };

        /// <summary>
        /// constructor of StartScene
        /// load fonts and backgrounds
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        public StartScene(Game game, SpriteBatch spriteBatch) : base(game)
        {
            this.spriteBatch = spriteBatch;
            //TODO: construct any game components here
            Texture2D bgForestTex = game.Content.Load<Texture2D>("Image/Background/BackgroundForest");
            Texture2D bgCityTex = game.Content.Load<Texture2D>("Image/Background/BackgroundCity");
            SpriteFont regularFont = game.Content.Load<SpriteFont>("Font/regularFont");
            SpriteFont highlightFont = game.Content.Load<SpriteFont>("Font/highlightFont");

            Menu = new MenuComponent(game, spriteBatch, regularFont, highlightFont, menuItems, bgForestTex, bgCityTex);
            this.Components.Add(Menu);
        }
    }
}
