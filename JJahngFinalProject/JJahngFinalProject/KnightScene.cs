﻿/*
 * Game1.cs
 * Final Project
 * Revision History
 *          Jay Jahng 12/05/2018: created
 *          Jay Jahng 12/10/2018: Finished
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace JJahngFinalProject
{
    /// <summary>
    /// Class for displaying Knight level
    /// </summary>
    public class KnightScene : GameScene
    {
        private SpriteBatch spriteBatch;
        private BackgroundScrolling bs;
        private GroundScrolling gs;
        private Character character;
        private SpawnManager spawnManager;
        private SoundEffect backgroundMusic;
        private SoundEffectInstance bgmInstance;

        /// <summary>
        /// current scenes Character object
        /// </summary>
        public Character Character { get => character; set => character = value; }

        /// <summary>
        /// backgroundmusic's instance to controll play, pause, stop
        /// </summary>
        public SoundEffectInstance BgmInstance { get => bgmInstance; set => bgmInstance = value; }

        /// <summary>
        /// KnightScene's constructor
        /// load all components for knight scene
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        public KnightScene(Game game,
            SpriteBatch spriteBatch) : base(game)
        {
            this.spriteBatch = spriteBatch;
            Texture2D idleTex = game.Content.Load<Texture2D>("Image/Character1/Idle");
            Texture2D moveTex = game.Content.Load<Texture2D>("Image/Character1/Move");
            Texture2D heartTex = game.Content.Load<Texture2D>("Image/Life/Heart");

            Texture2D woodTex = game.Content.Load<Texture2D>("Image/Obstacle/WoodenBox");
            Texture2D steelTex = game.Content.Load<Texture2D>("Image/Obstacle/SteelBox");

            Texture2D backgroundTex = game.Content.Load<Texture2D>("Image/Background/BackgroundForest");
            Texture2D groundTex1 = game.Content.Load<Texture2D>("Image/Ground/GroundDirt");
            Texture2D groundTex2 = game.Content.Load<Texture2D>("Image/Ground/GroundDirtFlag");
            SpriteFont scoreFont = game.Content.Load<SpriteFont>("Font/regularFont");

            SoundEffect forestFootstep = game.Content.Load<SoundEffect>("Sound/ForestFootstep");
            SoundEffect woodCrack = game.Content.Load<SoundEffect>("Sound/WoodCrack");           

            Vector2 groundPos = new Vector2(0, Share.stage.Y - groundTex1.Height);
            Vector2 charPos = new Vector2(200, 350 - idleTex.Height);
            
            bs = new BackgroundScrolling(game, spriteBatch, backgroundTex, Vector2.Zero, Share.speed / 3);
            gs = new GroundScrolling(game, spriteBatch, groundTex1, groundTex2, groundPos, Share.speed);
            character = new Character(game, spriteBatch, idleTex, moveTex, heartTex, charPos, scoreFont, forestFootstep);
            spawnManager = new SpawnManager(game, spriteBatch, this, woodTex, steelTex, character, woodCrack, 3, 8);
            
            this.Components.Add(bs);
            this.Components.Add(gs);
            this.Components.Add(character);
            this.Components.Add(spawnManager);
            
            backgroundMusic = game.Content.Load<SoundEffect>("Sound/ForestMusic");
            bgmInstance = backgroundMusic.CreateInstance();
            bgmInstance.IsLooped = true;
        }

        /// <summary>
        /// if background music was stopped or paused, resume
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (this.Enabled)
            {
                if (bgmInstance.State == SoundState.Stopped)
                {
                    bgmInstance.Play();
                }
                else if (bgmInstance.State == SoundState.Paused)
                {
                    bgmInstance.Resume();
                }
            }
            base.Update(gameTime);
        }
    }
}
